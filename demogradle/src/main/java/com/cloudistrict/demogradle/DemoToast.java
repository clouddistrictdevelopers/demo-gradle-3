package com.cloudistrict.demogradle;

import android.content.Context;
import android.widget.Toast;

public class DemoToast {
    public static void showToast(Context ctx, String msg){
        Toast.makeText(ctx, "Msg: " + msg, Toast.LENGTH_SHORT).show();
    }
}
